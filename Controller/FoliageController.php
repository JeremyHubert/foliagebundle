<?php

namespace Ifgm\FoliageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FoliageController extends Controller
{
    /**
     * Render the direct children of an element
     */
    public function showTreeAction($id)
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $entity        = $entityManager->getRepository('IfgmSiteBundle:Object')->getAdminChildren($id);

        return $this->render('IfgmSiteBundle:Admin:tree.html.twig', array('entity' => $entity));
    }

    /**
     * Re-order the object nestedset
     */
    public function orderTreeAction()
    {
        $request = $this->getRequest();
        $moves = json_decode(stripslashes($request->get('move')), true);
        $nodeManager = $this->get('ifgm_site.node_manager');

        foreach ($moves as $move) {
            if (!isset($move['id'])) {
                $move['id'] = null;
            }

            $nodeManager->moveNode($move['node'], $move['id'], $move['where']);
        }

        return new Response('Arborescence enregistrée avec succès. Rechargez la page pour réactiver les liens "voir".');
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function showTreeUnderAction()
    {
        $request       = $this->getRequest();
        $entityRepository = $this->get('doctrine.orm.entity_manager')->getRepository($request->get('objectClass'));
        $root          = $entityRepository->find($request->get('id'));
        $children      = $entityRepository->getAllChildren($request->get('id'));

        return $this->render('IfgmSiteBundle:Tree:children.html.haml', array('children' => $children));
    }
}
