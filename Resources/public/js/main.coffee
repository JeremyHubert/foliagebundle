(exports ? this).ifgmFoliage = (showTree, saveTree) ->
  doActions = ->
    $(".drop-here").droppable
      tolerance: "pointer"
      hoverClass: "hover-class"
      drop: (event, ui) ->

        # Check if the dropped object is not a parent of the drop zone // And check if  the object is draggable
        if not $(ui.draggable).html().match($(this).html()) and not $(ui.draggable).hasClass("locked") and not $(this).hasClass("locked")
          $(ui.draggable).clone().attr("style", "").prependTo $(this).parent().next("ul")
          $(ui.draggable).remove()
          # $(ui.draggable).sortable("option", "disabled", true).draggable "option", "disabled", true
          move[order++] =
            node: $(ui.draggable).attr("data-id")
            where: "into"
            id: $(this).parent().parent().attr("data-id")

          doActions()

    # Delete and restore sortable and draggable behaviours to restore link elements
    # Ugly timer needed to disable links before destroy behaviours
    $(".tree").sortable(
      placeholder: 'placeholder-block'
      distance: 5
      stop: (event, ui) ->
        $(this).animate
          opacity: 1
        , 100, ->
          $(this).sortable("destroy").draggable "destroy"
          if ui.item.index() is 0
            move[order++] =
              node: $(ui.item).attr("data-id")
              where: "into"
              id: $(ui.item).parent().parent().attr("data-id")
          else if ui.item.index() > 0
            move[order++] =
              node: $(ui.item).attr("data-id")
              where: "after"
              id: $(ui.item).prev().attr("data-id")
          doActions()

    ).draggable distance: 5

  move = {}
  order = 0
  doActions()
  $(".saveTree, .create_types li").button()
  $(".saveTree").click ->
    $.ajax
      url: saveTree
      type: "POST"
      data: "move=" + JSON.stringify(move)
      success: (data) ->
        $("#flash-notice").html(data).show("drop").delay(3000).hide "drop"
        move = {}
        order = 0


  $(document).on "click", ".leave_folder", ->
    element = $(this).parent().clone()
    parent = $(this).parent().parent().parent()
    console.log
      node: element.attr("data-id")
      where: "after"
      id: parent.attr("data-id")

    parent.after element
    $(this).parent().remove()
    move[order++] =
      node: element.attr("data-id")
      where: "after"
      id: parent.attr("data-id")

    doActions()

  $(document).on "click", ".toggle", (e) ->
    e.preventDefault()

    if $(this).hasClass("closed")
      $(this).removeClass("closed").parent().find("ul.tree:first").show "blind"
    else
      $(this).addClass("closed").parent().find("ul.tree:first").hide "blind"
    unless $(this).hasClass("loaded")
      tree = $(this)
      $(this).parent().find("img.loading").first().show()
      $.ajax
        url: $(this).attr("href")
        type: "GET"
        success: (data) ->
          tree.parent().find("ul.tree").append data
          tree.addClass "loaded"
          tree.parent().find("img.loading").first().hide "fast"
          
          # suppression des doublons
          $(".child", tree.parent()).each ->
            $(".child[data-id=\"" + $(this).attr("data-id") + "\"]").first().remove()  if $(".child[data-id=\"" + $(this).attr("data-id") + "\"]").size() > 1

          doActions()