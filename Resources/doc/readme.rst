# FoliageBundle

FoliageBundle est un ensemble de composants qui permettent de manipuler simplement une arborescence.

Il est possible de créer plusieurs arbres, de les afficher et d'effectuer des recherches de noeuds.