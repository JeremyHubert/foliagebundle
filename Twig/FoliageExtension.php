<?php

namespace Ifgm\FoliageBundle\Twig;

use Ifgm\FoliageBundle\Manager\TreeManager;
/**
 * Some tools to render tree
 *
 * @author Jérémy Hubert <jeremy.hubert@infogroom.fr>
 */
class FoliageExtension extends \Twig_Extension
{
    private $treeManager;

    /**
     * Constructor.
     *
     * @param SettingsManager $settings The settings manager
     */
    public function __construct($container)
    {
        $this->treeManager = $container->get('ifgm_foliage.tree_manager');
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return array(
            'html_tree' => new \Twig_Function_Method($this, 'getHtmlTree', array('is_safe' => array('html'))),
        );
    }

    /**
     * Returns the asked setting
     * @param string $key Key of asked setting
     *
     * @return mixed
     */
    public function getHtmlTree($objectClass, $id, $template = null, $renderedField = 'label')
    {
        return $this->treeManager->getHtmlTree($objectClass, $id, $template, $renderedField);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'foliage_extension';
    }
}
