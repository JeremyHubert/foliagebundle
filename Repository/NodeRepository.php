<?php

namespace Ifgm\FoliageBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * NodeRepository
 *
 * @author Jérémy Hubert <jeremy.hubert@ifgm.fr>
 */
class NodeRepository extends EntityRepository
{
    public function findAllButNot($entity) {
        $qb = $this->createQueryBuilder('h');
        $sub = $this->createQueryBuilder('h2')
            ->select('h2.id')
            ->where('h2.right < :entRight')
            ->andWhere('h2.left > :entLeft')
            ->orWhere('h2.id = :entId');

        $qb = $qb->setParameter('entLeft', $entity->getLeft())
            ->setParameter('entRight', $entity->getRight())
            ->setParameter('entId', $entity->getId())
            ->andWhere($qb->expr()->notIn('h.id', $sub->getDQL()))
            ->getQuery();

        return $qb->getResult();
    }

    public function findAllPositiveNodes() {
        $qb = $this->createQueryBuilder('h')
            ->addWhere('h.right > 0')
            ->getQuery();

        return $qb->getResult();
    }

    public function findOriginal($id)
    {
        $qb = $this->createQueryBuilder('h')
                  ->where('h.id = '.$id)
                  ->getQuery();

        return $qb->getResult();
    }

    public function getAvailableLanguages()
    {
        $qb=$this->createQueryBuilder('h')
            ->leftJoin('h.objectType', 'o')
            ->add('where', 'o.name = ?1')
                ->setParameter(1, 'Langue')
            ->getQuery();

            return $qb->getResult();
    }

    public function getOneByPath($pathOrId)
    {
        $path = explode('/', $pathOrId);
        $level = count($path)-1;
        $id = array($pathOrId);

        $qb = $this->createQueryBuilder('h')
                      ->addSelect('t')
                  ->leftJoin('h.objectType', 't')
                      ->addSelect('f')
                  ->leftJoin('h.objectFields', 'f')
                      ->addSelect('d')
                  ->leftJoin('t.fieldDefinition', 'd')
                  ->where("h.id = ?2")
                  ->orWhere("h.path = :path")
                  ->setParameter(2, $id[count($id)-1])
                  ->setParameter('path', $pathOrId)
                  ->setMaxResults(1);

        $qb = $qb->getQuery();

        return $qb->getResult();
    }

    public function getMenuChildren($id)
    {
        $qb = $this->createQueryBuilder('h');

        if ($id != null) {
            $qb->where('h.parent = '.$id)
                ->andWhere('h.menuShow = 1');
        } else {
            $qb->where('h.parent IS NULL');
        }

        $qb = $qb->andWhere('h.menuShow = 1')
            ->addSelect('t')
                ->leftJoin('h.objectType', 't')
            ->addSelect('f')
                ->leftJoin('h.objectFields', 'f')
            ->addSelect('d')
                ->leftJoin('t.fieldDefinition', 'd')
            ->orderBy('h.left')
            ->getQuery();

        return $qb->getResult();
    }

    public function getAdminChildren($id)
    {
        $qb = $this->createQueryBuilder('h');

        if ($id != null) {
            $qb->where('h.parent = '.$id);
        } else {
            $qb->where('h.parent IS NULL');
        }

        $qb = $qb
            ->orderBy('h.left')
            ->getQuery();

       return $qb->getResult();
    }

    public function getParentsAndNextObjects($object) {
        $qb = $this->createQueryBuilder('h')
            ->where('h.right > :right')
            ->setParameter('right', $object->getRight())
            ->getQuery();

        return $qb->getResult();
    }

    public function findNextRight($right)
    {
        $qb = $this->createQueryBuilder('h')
            ->where('h.right > :right')
            ->setParameter('right', $right)
            ->getQuery();

        return $qb->getResult();
    }

    public function getParentsAndNextObjectsNotChildren($dest, $entity) {
        $qb = $this->createQueryBuilder('h')
            ->where('h.right > :right')
            ->setParameter('right', $dest->getRight());

        $sub = $this->createQueryBuilder('h2')
            ->select('h2.id')
            ->where('h2.right < :entRight')
            ->andWhere('h2.left > :entLeft')
            ->orWhere('h2.id = :destId')
            ->orWhere('h2.id = :entId');

        $qb = $qb->setParameter('entLeft', $entity->getLeft())
            ->setParameter('entRight', $entity->getRight())
            ->setParameter('entId', $entity->getId())
            ->setParameter('destId', $dest->getId())
            ->andWhere($qb->expr()->notIn('h.id', $sub->getDQL()))
            ->getQuery();

        return $qb->getResult();
    }

    public function getDestParentsAndChildrenAndNextObjectsNotEntityChildren($dest, $entity) {
        $qb = $this->createQueryBuilder('h')
            ->where('h.right > :right')
            ->orWhere('h.left > :left')
            ->setParameter('right', $dest->getRight())
            ->setParameter('left', $dest->getLeft());

        $sub = $this->createQueryBuilder('h2')
            ->select('h2.id')
            ->where('h2.right < :entRight')
            ->andWhere('h2.left > :entLeft')
            ->orWhere('h2.id = :destId')
            ->orWhere('h2.id = :entId');

        $qb = $qb->setParameter('entLeft', $entity->getLeft())
            ->setParameter('entRight', $entity->getRight())
            ->setParameter('entId', $entity->getId())
            ->setParameter('destId', $dest->getId())
            ->andWhere($qb->expr()->notIn('h.id', $sub->getDQL()))
            ->getQuery();

        return $qb->getResult();
    }

    public function getParents($object) {
        $qb = $this->createQueryBuilder('h')
            ->where('h.left < :left')
                ->setParameter('left', $object->getLeft())
            ->where('h.right > :right')
                ->setParameter('right', $object->getRight())
            ->getQuery();

        return $qb->getResult();
    }

    public function getNextObjects($object) {
        $qb = $this->createQueryBuilder('h')
            ->where('h.left > :left')
            ->setParameter('left', $object->getLeft())
            ->getQuery();

        return $qb->getResult();
    }

    public function getNextObjectsNotChildren($object) {
        $qb = $this->createQueryBuilder('h')
            ->where('h.left > :left')
            ->andWhere('h.right > :right')
            ->setParameter('left', $object->getLeft())
            ->setParameter('right', $object->getRight())
            ->getQuery();

        return $qb->getResult();
    }

    public function getNextObjectsNotEntity($object, $entity) {
        $qb = $this->createQueryBuilder('h')
            ->where('h.left > :left')
            ->setParameter('left', $object->getLeft());

        $sub = $this->createQueryBuilder('h2')
            ->select('h2.id')
            ->where('h2.right < :entRight')
            ->andWhere('h2.left > :entLeft')
            ->orWhere('h2.id = :entId');

        $qb = $qb->setParameter('entLeft', $entity->getLeft())
            ->setParameter('entRight', $entity->getRight())
            ->setParameter('entId', $entity->getId())
            ->andWhere($qb->expr()->notIn('h.id', $sub->getDQL()))
            ->getQuery();

        return $qb->getResult();
    }

    public function getChildren($object) {
        return $this->getChildrenAtMaxLevel($object, 0);
    }

    public function getAllChildrenQueryBuilder($object) {

        $qb = $this->createQueryBuilder('h')
            ->setParameter('left', $object->getLeft())
            ->setParameter('right', $object->getRight());

        if ($object->getRight() > 0) {
            $qb = $qb->where('h.left > :left')
                ->andWhere('h.right < :right');
        } else {
            $qb = $qb->where('h.left < :left')
                ->andWhere('h.right > :right');
        }

        return $qb;
    }


    public function getAllChildren($object) {

        return $this->getAllChildrenQueryBuilder($object)
            ->getQuery()
            ->getResult();
    }


    public function getAllChildrenOrderedByLevel($object) {

        return $this->getAllChildrenQueryBuilder($object)
            ->addOrderBy('h.left')
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function getChildrenAtMaxLevel($object = null, $level = 1, $show = array()) {
        $qb = $this->createQueryBuilder('h');

        if (is_numeric($object)) {
            $object = $this->find($object);
        }
        // Set max level
        if ($object) {
            $maxlevel = $object->getLevel() + $level;
            $qb->andWhere('h.left > :left')
                ->andWhere('h.right < :right')
                ->setParameter('left', $object->getLeft())
                ->setParameter('right', $object->getRight());
        } else {
            $maxlevel = $level - 1;
        }

        // Select objects to show
        if (count($show)>0) {
            $sub = $this->getEntityManager()->getRepository('Ifgm\SiteBundle\Entity\ObjectType')
                ->createQueryBuilder('h2');

            foreach ($show as $key => $slug) {
                $sub->orWhere('h2.slug = ?'.$key);
                $qb->setParameter($key, $slug);
            }

            $qb->andWhere($qb->expr()->in('h.objectType', $sub->getDQL()));
        }

        $qb = $qb->andWhere('h.level <= :level')
            ->setParameter('level', $maxlevel)
            ->orderBy('h.left');

        $qb = $qb->getQuery();

        return $qb->getResult();
    }

    public function prepareQuery($qb) {
        $qb = $qb->addSelect('t')
                ->leftJoin('h.objectType', 't')
            ->addSelect('f')
                ->leftJoin('h.objectFields', 'f')
            ->addSelect('d')
                ->leftJoin('t.fieldDefinition', 'd')
            ->getQuery();

        return $qb;
    }
}
