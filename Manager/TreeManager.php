<?php

namespace Ifgm\FoliageBundle\Manager;

/**
 * Description of TreeManager
 *
 * @author infogroom
 */
class TreeManager
{
    /**
     * The Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Service Container
     * @var Service Container
     */
    protected $container;

    public function __construct(\Doctrine\ORM\EntityManager $em, $container)
    {
      $this->em = $em;
      $this->container = $container;
    }


    public function renderSubTree($id, $namespace)
    {
        $entity = $this->em->getRepository($namespace)->getAdminChildren($id);

        return $this->container->get('templating')->render('IfgmFoliageBundle:Default:tree.html.haml', array('entity' => $entity));
    }

    /**
     * Execute the queue of movements given as parameters
     *
     * @param string $moves Json encoded movements
     * @param string $namespace namespace of the entity to manage
     *
     * @return \Ifgm\FoliageBundle\Manager\Response
     */
    public function orderTree($moves, $namespace)
    {
        $moves = json_decode(stripslashes($moves), true);
        $entityRepository = $this->em->getRepository($namespace);
        $nodeManager = new NodeManager($entityRepository);

        foreach ($moves as $move) {
            if (!isset($move['id'])) {
                $move['id'] = null;
            }

            $nodeManager->moveNode($move['node'], $move['id'], $move['where']);
        }
    }

    public function getHtmlTree($objectType, $id, $template = null, $renderedField = 'label')
    {
        $template = $template ? $template : 'IfgmFoliageBundle:Tree:children.html.haml';
        $entity = $this->em->getRepository($objectType)->find($id);
        $children = $this->em->getRepository($objectType)->getAllChildrenOrderedByLevel($entity);

        $trees = $this->toHierarchy($children);

        return $this->container->get('templating')->render($template, array('children' => $trees, 'renderedField' => $renderedField, 'template' => $template));
    }

    public function toHierarchy($collection)
    {
        // Trees mapped
        $trees = array();
        $l = 0;

        if (count($collection) > 0) {
            // Node Stack. Used to help building the hierarchy
            $stack = array();

            foreach ($collection as $item) {
                $item['__children'] = array();

                // Number of stack items
                $l = count($stack);

                // Check if we're dealing with different levels
                while($l > 0 && $stack[$l - 1]['level'] >= $item['level']) {
                        array_pop($stack);
                        $l--;
                }

                // Stack is empty (we are inspecting the root)
                if ($l == 0) {
                        // Assigning the root node
                        $i = count($trees);
                        $trees[$i] = $item;
                        $stack[] = & $trees[$i];
                } else {
                        // Add node to parent
                        $i = count($stack[$l - 1]['__children']);
                        $stack[$l - 1]['__children'][$i] = $item;
                        $stack[] = & $stack[$l - 1]['__children'][$i];
                }
            }
        }

        return $trees;
    }
}
