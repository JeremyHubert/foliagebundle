<?php

namespace Ifgm\FoliageBundle\Manager;

use Doctrine\ORM\EntityRepository;

/**
 * A class that retrieves the identifiers of properties that must be displayed on the homepage.
 *
 * @author Jérémy Hubert <jeremy.hubert@infogroom.fr>
 */
class NodeManager
{
    protected $entityManager;
    protected $repository;

    public function __construct(EntityRepository $repository) {
        $this->repository    = $repository;
        $this->entityManager = $repository->createQueryBuilder('getEm')->getEntityManager();
    }

    /**
     * moveNode
     *
     * @param mixed $node Node object || id integer
     * @param mixed $destination Node object || id integer || null (=> root)
     * @param String $where = 'after' || 'into'
     * @return void
     */
    public function moveNode($node, $destination = null, $where = 'after')
    {
        // Create objects
        if (is_numeric($node)) {
            $node = $this->repository->find($node);
        }

        if (is_numeric($destination)) {
            $destination = $this->repository->find($destination);
        }

        if ($destination) {
            // Don't do anything if node isn't moved
            if ($where == 'into' && $node->getLeft() == $destination->getLeft() + 1) {

                return false;
            } elseif ($where == 'after' && $node->getLeft() == $destination->getRight() + 1) {

                return false;
            }
        }

        $this->softDelete($node);
        $this->insertNode($node, $destination, $where);
        $this->updatePath($node);
        $this->entityManager->flush();
    }

    public function softDelete($node)
    {
        // Move node and children to *-1 left and right
        $children = $this->repository->getAllChildren($node);

        foreach ($children as $child) {
            $child->setLeft(-$child->getLeft());
            $child->setRight(-$child->getRight());
            $this->entityManager->persist($child);
        }

        $node->setLeft(-$node->getLeft());
        $node->setRight(-$node->getRight());
        $this->entityManager->persist($node);
        $this->entityManager->flush();
        $this->mergeAfterDelete($node);
    }

    public function insertNode($node, $destination = null, $where = 'after')
    {
        if (null === $node->getLeft()) {
            $node->setLeft(1);
        }

        $nodeSize = $this->getNodeSize($node);
        // Make place to receive it
        if (!$destination) {
            $nodesToMove = $this->repository->findNextRight(0);
            $offset = 1;
        } elseif ($where == 'into') {
            $nodesToMove = $this->repository->findNextRight($destination->getLeft());
            $offset = $destination->getLeft()+1;
        } elseif ($where == 'after') {
            $nodesToMove = $this->repository->findNextRight($destination->getRight());
            $offset = $destination->getRight()+1;
        }

        foreach ($nodesToMove as $nodeToMove) {
            if (!$destination
                || $nodeToMove->getId() != $node->getId()
            ) {
                // Move left only if needed
                if (!$destination || $nodeToMove->getLeft() > $destination->getLeft()) {
                    $nodeToMove->setLeft($nodeToMove->getLeft() + $nodeSize + 1);
                }

                $nodeToMove->setRight($nodeToMove->getRight() + $nodeSize + 1);
                $this->entityManager->persist($nodeToMove);
            }
        }
        $this->entityManager->flush();

        // Place tree
        $children = $this->repository->getAllChildren($node);

        $left = $node->getLeft();
        $node->setLeft($offset);
        $node->setRight(- $node->getRight() + $left + $offset);

        $this->entityManager->persist($node);
        $this->entityManager->flush();

        // Update node level and parent
        if(!$destination) {
            $addLevel = - $node->getLevel();
            $node->setLevel(0);
        } elseif ($where == 'into') {
            $addLevel = $destination->getLevel() - $node->getLevel() + 1;
            $node->setParent($destination);
            $node->setLevel($destination->getLevel()+1);
        } else {
            $addLevel = $destination->getLevel() - $node->getLevel();
            $node->setParent($destination->getParent());
            $node->setLevel($destination->getLevel());
        }

        foreach ($children as $child) {
            $child->setLeft(- $child->getLeft() + $left + $offset);
            $child->setRight(- $child->getRight() + $left + $offset);
            $child->setLevel($child->getLevel() + $addLevel);

            $this->entityManager->persist($child);
        }

        $this->entityManager->flush();
        $this->updatePath($node);
    }

    public function getNodeSize($node)
    {
        if (!$node->getLeft() || !$node->getRight()) {
            return 1;
        }

        return abs($node->getRight() - $node->getLeft());
    }

    /**
     * updatePath update path and translated path for node and children
     *
     * @param unknown $node
     * @return void
     */
    public function updatePath($node)
    {
        $children = $this->repository->getAllChildren($node);
        if($node->getParent()) {
            $node->setPath($node->getParent()->getPath().'/'.$node->getSlug());
        } else {
            $node->setPath($node->getSlug());
        }

        $this->entityManager->persist($node);

        foreach ($children as $child) {
            $child->setPath($child->getParent()->getPath().'/'.$child->getSlug());
            //$this->entityManager->flush();
            $this->entityManager->persist($child);
        }

        $this->entityManager->flush();
    }

    /**
     * mergeAfterDelete regroup lft and rgt after a node delete
     *
     * @param Node $node Deleted node
     */
    public function mergeAfterDelete($node)
    {
        $left = abs($node->getLeft());
        $nodeSize = $this->getNodeSize($node);

        $nodesToReplace = $this->repository->findNextRight($left);

        foreach ($nodesToReplace as $node) {
            // Move left only if this is not a parent node
            if ($node->getLeft() >= $left) {
                $node->setLeft($node->getLeft() - $nodeSize - 1);
            }

            $node->setRight($node->getRight() - $nodeSize - 1);
            $this->entityManager->persist($node);
        }

        $this->entityManager->flush();
    }

    /**
     * Converts a request result into a tree
     *
     * @param array $nodes an array of nodes ordered by left desc
     * @see http://stackoverflow.com/questions/1606788/php-form-a-multi-dimensional-array-from-a-nested-set-model-flat-array
     */
    function toTree(&$array, &$previous = null, $level = 0)
    {
        $current = array();
        while ($node = current($array)) {
            if ($node->getLevel() < $level) {
                return $current;
            } elseif ($node->getLevel() > $level) {
                $previous = toTree($array, $current, $level + 1);
            } else {
                $current[$node->getId()] = $node['name'];
                $previous = &$current[$node->getId];
                next($array);
            }
        }

        return $current;
    }
}
